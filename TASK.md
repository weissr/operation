## Task for operational

### Login to remote server {ip}
Connect over SSH protocol with user "ec2-user" using privatekey

#### Launch current application on linux server
User should be able to access application by http://{ip}:8080 or http://{sub_domain}.weissr-cloud.com:8080


#### Make secure connection to application (https)
User should be able to access application only by secure connection https://{sub_domain}.weissr-cloud.com
* you can use self signed certificate or Letsencrypt


#### Customize application
Make custom output when we open landing page of application https://homework.weissr-cloud.com

"Hello from Weissr!" -> "Hello from new employee" 