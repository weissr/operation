# Test application
A spring boot application

## Usage
- Using maven 
```
mvn spring-boot:run
```

- Or running as jar
```
mvn clean install
java -jar target/ops-0.0.1-SNAPSHOT.jar
```
Go to Browser and type http://localhost:8080/

### Technology stack
- maven https://maven.apache.org/guides/index.html
- maven-wrapper https://www.baeldung.com/maven-wrapperappache 
- spring-boot https://spring.io/projects/spring-boot
- java 11 - https://openjdk.java.net/
